﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;

namespace Decryptor
{
    public partial class Form1 : Form
    {
        private Form2 _Form2 = new Form2();
        public Form1()
        {
            InitializeComponent();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {

                if (Path.GetExtension(openFileDialog1.FileName)==".docx")
                {                    
                    Word.Application wordapp = new Word.Application
                    {
                        Visible = false
                    };
                    Word.Document worddocument = wordapp.Documents.Open(openFileDialog1.FileName, Type.Missing, Type.Missing);
                    object start = 0;
                    object stop = worddocument.Characters.Count;
                    Word.Range Rng = worddocument.Range(ref start, ref stop);
                    richTextBox1.Text = Rng.Text;
                    wordapp.Quit();
                }
                else if (Path.GetExtension(openFileDialog1.FileName) == ".txt")
                {
                    StreamReader sr = new StreamReader(openFileDialog1.FileName);
                    richTextBox1.Text = sr.ReadToEnd();                    
                    sr.Close();
                }
                else
                {
                    MessageBox.Show("Неверный тип файла. Для работы доступны форматы .txt и .docx");
                }   
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            richTextBox2.Text = CryptoStep(richTextBox1.Text, (int)numericUpDown1.Value);
        }

        public static string CryptoStep(string s, int step) //функция шифрования
        {
            string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string alphabetUpper = alphabet.ToUpper();
            string alphabetNumbers = "0123456789";
            StringBuilder stringBuilder = new StringBuilder(s);
            try
            {
                for (int i = 0; i < s.Length; i++)
                {
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        if (s[i] == alphabet[j])
                        {
                            if ((j + step) < 0)
                            {
                                int index = (j + step) + alphabet.Length;
                                stringBuilder[i] = alphabet[index];
                            }
                            else
                            {
                                stringBuilder[i] = alphabet[(j + step) % alphabet.Length];
                            }
                        }
                        if (s[i] == alphabetUpper[j])
                        {
                            if ((j + step) < 0)
                            {
                                int index = (j + step) + alphabet.Length;
                                stringBuilder[i] = alphabetUpper[index];
                            }
                            else
                            {
                                stringBuilder[i] = alphabetUpper[(j + step) % alphabet.Length];
                            }
                        }
                    }
                    for (int j = 0; j < alphabetNumbers.Length; j++)
                    {
                        if (s[i] == alphabetNumbers[j])
                        {
                            if ((j + step) < 0)
                            {
                                int index = (j + step) + alphabetNumbers.Length;
                                stringBuilder[i] = alphabetNumbers[index];
                            }
                            else
                            {
                                stringBuilder[i] = alphabetNumbers[(j + step) % alphabetNumbers.Length];
                            }
                        }
                    }
                }                
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("передано значение null");                
            }
            return stringBuilder.ToString();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox2.Text = CryptoStep(richTextBox1.Text, (int)numericUpDown1.Value);
        }

        private void исходныйТекстToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            if (saveFileDialog1.FilterIndex == 1) //txt 
            {
                File.WriteAllText(saveFileDialog1.FileName, richTextBox2.Text);
            }
            if (saveFileDialog1.FilterIndex == 2) //docx
            {
                Word.Application wordapp = new Word.Application
                {
                    Visible = false
                };
                Word.Document worddocument = wordapp.Documents.Add();
                object start = 0;
                Word.Range Rng = worddocument.Range(ref start);
                Rng.Text = richTextBox2.Text;
                worddocument.SaveAs2(saveFileDialog1.FileName);
                wordapp.Quit();
            }
            MessageBox.Show("Файл сохранен");
        }

        private void текстПослеСдвигаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            if (saveFileDialog1.FilterIndex == 1) //txt 
            {
                File.WriteAllText(saveFileDialog1.FileName, richTextBox2.Text);
            }
            if (saveFileDialog1.FilterIndex == 2) //docx
            {
                Word.Application wordapp = new Word.Application
                {
                    Visible = false
                };
                Word.Document worddocument = wordapp.Documents.Add();
                object start = 0;                
                Word.Range Rng = worddocument.Range(ref start);
                Rng.Text = richTextBox2.Text;
                worddocument.SaveAs2(saveFileDialog1.FileName);
                wordapp.Quit();
            }
            MessageBox.Show("Файл сохранен");
        }

        private void зашифроватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                string parsingText = "";
                decimal step = 0;
                if (Path.GetExtension(openFileDialog2.FileName) == ".docx")
                {
                    Word.Application wordapp = new Word.Application
                    {
                        Visible = false
                    };
                    Word.Document worddocument = wordapp.Documents.Open(openFileDialog2.FileName, Type.Missing, Type.Missing);
                    object start = 0;
                    object stop = worddocument.Characters.Count;
                    Word.Range Rng = worddocument.Range(ref start, ref stop);                    
                    DialogResult dialogResult = _Form2.ShowDialog(this);
                    step = _Form2.cryptoStep;
                    if (dialogResult == DialogResult.OK)
                    {
                        parsingText = Rng.Text;
                        Rng.Text = CryptoStep(parsingText, (int)step);                        
                        worddocument.SaveAs2(openFileDialog2.FileName);
                        wordapp.Quit();
                        MessageBox.Show("Файл зашифрован!");
                    }
                }
                else if (Path.GetExtension(openFileDialog2.FileName) == ".txt")
                {
                    StreamReader sr = new StreamReader(openFileDialog2.FileName);
                    parsingText = sr.ReadToEnd();
                    sr.Close();
                    DialogResult dialogResult =  _Form2.ShowDialog(this);
                    
                    if (dialogResult == DialogResult.OK)
                    {
                        step = _Form2.cryptoStep;                        
                        File.WriteAllText(openFileDialog2.FileName, CryptoStep(parsingText, (int)step));
                        MessageBox.Show("Файл зашифрован!");
                    }                                        
                    
                }
                else
                {
                    MessageBox.Show("Неверный тип файла. Для работы доступны форматы .txt и .docx");
                }
            }
        }
        //Справка
        private void описаниеПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            StreamReader sr = new StreamReader("..\\..\\readme.txt");
            string parsingText = sr.ReadToEnd();
            MessageBox.Show(parsingText);
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader("..\\..\\shortinfo.txt");
            string parsingText = sr.ReadToEnd();
            MessageBox.Show(parsingText);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
    }
}
