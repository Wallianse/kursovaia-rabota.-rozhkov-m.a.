﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decryptor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decryptor.Tests
{
    [TestClass()]
    public class Form1Tests
    {
        [TestMethod()]
        public void CryprtoStepTest_VaildParameter()
        {
            string testString = "абв";
            string result = Form1.CryptoStep(testString,2);
            string testString2 = "вгд";
            Assert.AreEqual(result,testString2);
        }

        [TestMethod()]
        public void CryprtoStepTest_NullString()
        {
            string testString = null;
            string result = Form1.CryptoStep(testString, 10000);           
            
        }
    }
}